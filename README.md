Sobre o tema para edX
=============
Este tema é baseado no tema padrão do Open edX feito pela Stanford.


Como atualizar o tema na production stack
========================================

* Conecte via SSH
* Edite o arquivo /edx/app/edx_ansible/server-vars.yml: `sudo nano /edx/app/edx_ansible/server-vars.yml`

* Cole o texto:

`

    edxapp_use_custom_theme: true

    edxapp_theme_name: 'default'

    edxapp_theme_source_repo: 'https://ftonon@bitbucket.org/ftonon/ncr-edx-theme-test.git'

    edxapp_theme_version: 'HEAD'

`

* Salve o arquivo e feche o editor

* Execute o comando: `sudo /edx/bin/update edx-platform master`
